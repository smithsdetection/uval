# Code of Conduct

We expect project participants to adhere to similar Codes of Conduct as Facebook open open projects do. Please read the [full text](https://opensource.fb.com/code-of-conduct/) so that you can understand what actions will and will not be tolerated.
