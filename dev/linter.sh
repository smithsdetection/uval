#!/bin/bash -e
# Run this script at project root by "./dev/linter.sh" before you commit

{
  black --version | grep -E "20.8b1" > /dev/null
} || {
  echo "Linter requires 'black==20.8b1' !"
  exit 1
}

ISORT_VERSION=$(isort --version-number)
if [[ "$ISORT_VERSION" != 5.7* ]]; then
  echo "Linter requires isort==5.7.0 !"
  exit 1
fi

set -v

echo "Running isort ..."
isort -v src

echo "Running black ..."
python -m black --config=pyproject.toml src

echo "Running flake8 ..."
python -m flake8 --config=.flake8

echo "Running mypy ..."

mypy src/uval


command -v arc > /dev/null && arc lint