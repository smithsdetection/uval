# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import pathlib
import sys
from typing import List

import shutil
from myst_parser.sphinx_parser import MystParser

sys.path.insert(0, os.path.abspath("../uval"))


uval_src_folder = pathlib.Path(__file__).parents[2]
'''
print("Copying README.md to docs/source/development_setup.md")
shutil.copyfile(
    str(uval_src_folder / ".." / "README.md"), str(uval_src_folder / "docs" / "source" / "development_setup.md")
 )
with open('development_setup.md','r') as fr, open('setup.md','w') as fw:
    readme = fr.read()
    readme.replace('gif','png',2)
    fw.write(readme)
os.remove("development_setup.md")
'''
# -- Project information -----------------------------------------------------

project = "Unified Evaluation Framework for 3D X-Ray Data"
copyright = "2022, Smiths Detection"
author = "Philipp Fischer, Mohammad Razavi, Faraz Saeedan"

# The full version, including alpha/beta/rc tags
release = "0.6.0"


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.coverage",
    "sphinx.ext.napoleon",
    "myst_parser",
    "sphinx.ext.viewcode",
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]

source_suffix = {
    ".rst": "restructuredtext",
    ".md": "markdown",
}

source_parsers = {
    ".md": MystParser,
}
master_doc = "index"
# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []  # type: List[str]

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = "sphinx_rtd_theme"
html_theme_options = {
    "prev_next_buttons_location": "both",
}


# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ["../_static"]
