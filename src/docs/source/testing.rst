=======
Testing
=======

To run install dependencies and run tests, use this command::

    python -m pytest

Multiple Python Versions
========================

To run the tests on all the versions of Python uval supports, install
tox::

    pip install tox

Then, run the tests::

    tox