===============
Version History
===============

.. automodule:: uval
   :noindex:

0.1.7
------

* Bug fixes
    * Some documentation issues were fixed
    * Python 3.9 support was restored.