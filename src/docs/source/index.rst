.. Unified Evaluation Framework for 3D X-Ray Data documentation master file, created by
   sphinx-quickstart on Tue Jul  6 10:15:07 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Unified Evaluation Framework for 3D X-Ray Data's documentation!
==========================================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Helpful data
==================
.. toctree::
    :maxdepth: 1

    setup
    Datasplit_YAML_Format
    HDF5_Format
    license
    testing
    versions
