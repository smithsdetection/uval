# Copyright (c) Facebook, Inc. and its affiliates.
from .config_utils import default_argument_parser, get_cfg

__all__ = ["get_cfg", "default_argument_parser"]
