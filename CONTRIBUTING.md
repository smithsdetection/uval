# Contributing to Uval

## Issues

We use Gitlab issues to track public bugs and questions. Please make sure to follow one of the issue templates when reporting any issues.

## Pull Requests

We actively welcome pull requests.

However, if you’re adding any significant features (e.g. > 50 lines), please make sure to discuss with maintainers about your motivation and proposals in an issue before sending a PR. This is to save your time so you don’t spend time on a PR that we’ll not accept.

Things we consider before accepting pull requests:

* Whether the proposed solution has a good design / interface. This can be discussed in the issue prior to PRs, or in the form of a draft PR.

* Whether the proposed solution adds extra mental/practical overhead to users who don’t need such feature.

* Whether the proposed solution breaks existing APIs.

To add a feature to an existing function/class Func, there are always two approaches: (1) add new arguments to Func; (2) write a new Func_with_new_feature. To meet the above criteria, we often prefer approach (2), because:

* It does not involve modifying or potentially breaking existing code.

* It does not add overhead to users who do not need the new feature.

* Adding new arguments to a function/class is not scalable w.r.t. all the possible new research ideas in the future.

When sending a PR, please do:

* If a PR contains multiple orthogonal changes, split it to several PRs.

* If you’ve added code that should be tested, add tests.

* For PRs that need experiments (e.g. adding a new model or new methods), provide experiment results in the description of the PR.

* If APIs are changed, update the documentation.

* We use the Google style docstrings in python.

* Make sure your code lints with ./dev/linter.sh.

## License

By contributing to Uval, you agree that your contributions will be licensed under the LICENSE file in the root directory of this source tree.
