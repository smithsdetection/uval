#To enable a Conda virtual environment as a kernel in Jupyter, the following commands need to be executed:
conda activate uvalenv
(uvalenv) $ pip install ipykernel
(uvalenv) $ ipython kernel install --user --name=uvalenv